
// send by Henning Gross from the Noun Project


require('dotenv').config();
const puppeteer = require('puppeteer');
const AWS = require('aws-sdk');
const fs = require('fs');

AWS.config.loadFromPath('./aws.json');
console.log('Loaded AWS config');


var filename = process.env.DASHBOARD_FILENAME+'.png';
var screenShotPath = process.env.DASHBOARD_FILEPATH + '/' + filename;

console.log("Creating a new screenshot for: " +process.env.DASHBOARD_LOGIN_TYPE);
console.log("Storing in: " + screenShotPath);

/** open the browser and start in full screen app mode. See Chromium Command Line switches
  - https://peter.sh/experiments/chromium-command-line-switches/


  Set local policy for Chromium
  - Linux: https://www.chromium.org/administrators/linux-quick-start
  - Mac: copy org.chromium.Chromium.plist to /Library/Preferences (requires root)

**/
(async () => {

  const browser = await puppeteer.launch({
    headless: false,
    args: ['--disable-infobars', '--disable-session-crashed-bubble', '--noerrdialogs'],
  });

  const page = await browser.newPage();
  await page.setViewport({
    width: 1920,
    height: 1150,
    isLandscape: true,
  });

  //const loginPageReady = page.waitForFunction('input#identifierId');
  switch( process.env.DASHBOARD_LOGIN_TYPE) {

    case 'datastudio' :
      await page.goto(process.env.DASHBOARD_PAGE);

      // let's login
      const elementHandle = await page.$('input#identifierId');
      await elementHandle.type( process.env.DASHBOARD_USER );
      await page.click("#identifierNext");
      await page.waitForNavigation({ timeout: 5000, waitUntil: 'networkidle0'});
      
      const pw = await page.$('input[type=password]');
      await pw.type(process.env.DASHBOARD_PASS);

      //
      await Promise.all([
         await page.click("div#passwordNext"),
         await page.waitForSelector('suite-header'),
         // await page.waitForNavigation({ waitUntil: 'networkidle2' }),
         await page.waitFor(3000)
      ]);
      break;
    case 'newrelic' :
      // handle new relic here
    default :
      break;
  }

  await page.screenshot({
    path: screenShotPath,
    fullPage: true,
  });
  await browser.close();


  console.log('pushing to S3: ' + screenShotPath)
  // upload the file to S3
  var s3 = new AWS.S3();                                                             
  let fileData = fs.readFileSync(screenShotPath);                                        
  var params = {
    Bucket: process.env.AWS_S3_BUCKET,
    Key: filename,
    Body: fileData,
    ACL:'public-read'
  };
  console.log("Pushing image to S3");
  s3.putObject(params, function (perr, pres) {
    if (perr) {
      console.log("Error: ", perr);                                                
    } else {
      console.log("Done! :)");                        
    }                      
  });    


})();





